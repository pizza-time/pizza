<?php
/**
 * @var UserController $this
 * @var GlobalUser $model
 * @var CActiveForm $form
 * @var integer $return
 */
?>
<span class="ubi-message">Користувач активований. Перенаправлення на головну сторінку...</span>
<script>
    document.location="/";
</script>