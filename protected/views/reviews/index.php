<?php
/**
 * @var View $this
 * @var array $model
 * @var ReviewsForm $model1
 */
//$this->title = 'My Yii Application';

use app\components\utils\ImageUtils;
use app\models\Avatars;
use app\models\form\ReviewsForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;

$this->title = 'Відгуки';

?>

<h1><?=$this->title?></h1>

<? Pjax::begin(["id" => "dashboard-admin-pjax",  "timeout" => false,
    "options" => ["class" => "pjax"],
    'enablePushState' => false,
    "clientOptions" => [
        "push" => false,
    ]]);
?>

<div id="create" class="btn btn-success">Створити відгук</div>

<div class="reviews">
    <? foreach ($model as $mod):
        /**@var ReviewsForm $mod*/?>
        <div class="img">
            <? $img = Avatars::findOne($mod->imageId) ?>
            <img src="<?= $img?$img->image:''?>">
        </div>

        <div class="review">
            <div class="title-review">
                <?= $mod->userName; ?>
            </div>

            <div class="desc">
                <?= $mod->review; ?>
            </div>
        </div>

    <? endforeach; ?>
</div>

<div class="creating hidden">
    <div class="create-review">
        <? $form = ActiveForm::begin(['options' => ['action' => '/reviews']]); ?>

        <?= $form->field($model1, 'id')->hiddenInput() ?>

        <?= $form->field($model1, 'userName')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model1, 'review')->textarea(['maxlength' => true]) ?>
        <?= $form->field($model1, 'imageId')->hiddenInput()->label('Встановити картинку користувача'); ?>
        <label class="image-main" style="margin-bottom: 10px; cursor:pointer;">
            <? if( ! isset($model->imageId) || empty($model->imageId)){
                echo Html::img(Url::to('/images/no-photo.png'), ['id' => 'image-product']);
            } else {
                echo Html::img(Url::to(ImageUtils::genImageUrl($model->imageId, null, 200, 130), ['id' => 'image-product']));
            }
            ?>
            <?= Html::fileInput('image-jpg'); ?>
        </label>
        <div class="form-group">
            <?= Html::submitButton('Зберегти', ['class' =>'btn btn-success submit-review']) ?>
        </div>
        <img class="load-img hidden" style="margin-top: 20px" src="/images/load.gif" />
        <? ActiveForm::end(); ?>
    </div>
</div>

<script>
    $(function () {
        $('#create').click(function () {
            $('.creating').removeClass('hidden');
        });

        $('.submit-review').click (function () {
            $('.create-review .form-group, .image-main').addClass('hidden');
            $('.load-img').removeClass('hidden');
        })
    })
</script>

<script>
    $(function () {
        $('input[name="image-jpg"]').on('change', function () {
            var type = $('#reviewsform-name').attr('data-type'),
                id = $('#reviewsform-id').val();

            var data = new FormData();
            $.each( this.files, function( key, value )
            {
                data.append(key, value);
            });
            data.append('type', 'review');
            data.append('id', id);
            data.append('w', 66);
            data.append('h', 66);

            $.ajax({
                url: '/image/create-image',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
            }).done(function (data) {

                $('#reviewsform-imageid').val(data.id);
                $('#image-product').attr('src', data.src)

            }).fail(function (jqXHR, textStatus, errorThrown) {
                alert("Невозможно загрузить: Ошибка: " + jqXHR.status + " " + jqXHR.statusText);
            });
        });
    })
</script>

<? Pjax::end(); ?>